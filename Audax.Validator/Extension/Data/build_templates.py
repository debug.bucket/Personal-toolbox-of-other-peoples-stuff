import pathlib
from jinja2 import Template
import sys

files = list(pathlib.Path('.').glob('**/*.template'))

do_output = False
if any(arg == '--do-output' for arg in sys.argv):
	do_output = True

for file in files:
	with file.open() as f:
		in_text = f.read()

	template = Template(in_text)
	out_text = template.render(name='John Doe')
	
	parts = list(file.parts)
	parts[-1] = parts[-1].replace('.template', '')
	out_path = pathlib.Path('/'.join(parts))
	if do_output:
		with out_path.open('w') as f:
			f.write(out_text)
